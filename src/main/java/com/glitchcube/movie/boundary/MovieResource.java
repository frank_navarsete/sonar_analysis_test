package com.glitchcube.movie.boundary;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author glitchcube.com
 */
@Path("movie")
public class MovieResource {

    @Inject
    MovieManager movieManager;

    @GET
    public String fetchRegistrationForm() {
        return movieManager.fetchMovieNameByID(1);
    }

    private String uselessMethod() {
        return "";
    }
}
