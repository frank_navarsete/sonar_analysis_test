package com.glitchcube.movie.entity;

/**
 * @author glitchcube.com
 */
public class Movie {

    String title;
    Integer length;

    public Movie(String title) {
        this.title = title;
    }

    public Movie(String title, Integer length) {
        this.title = title;
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                '}';
    }
}
